package de.awacademy.stages;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Board;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class StageBuilder {

    private LinkedList<Stage> stages = new LinkedList<>();
    private Stage currentStage;

    public StageBuilder() {
        loadStages();
        currentStage = stages.poll();
    }

    private void loadStages(){
        //needs to add an instance of every existing Stage
        stages = new LinkedList<>();
        stages.add(new Stage1());
        stages.add(new Stage2());
        stages.add(new Stage3());
    }

    public List<Block> buildBlocks() {
        return currentStage.buildBlocks();
    }

    public Board buildBoard() {
        return currentStage.buildBoard();
    }

    public Ball buildBall() {
        return currentStage.buildBall();
    }


    /**
     * sets the currents stage to the next stage
     */
    public void nextStage() {
        currentStage = stages.poll();
    }

    /**
     * sets the currents stage to the first stage
     */
    public void reset() {
        loadStages();
        currentStage = stages.poll();
    }

    /**
     * checks for next stage
     *
     * @return true, if there is a next stage, else false
     */
    public boolean hasNextStage() {
        return (!(stages.peek() == null));
    }

    public List<Stage> getStages() {
        return (List<Stage>) stages;
    }

    public Stage getCurrentStage() {
        return currentStage;
    }
}
