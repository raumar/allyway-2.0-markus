package de.awacademy.stages;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Movement;
import de.awacademy.game_utilities.Point;
import de.awacademy.model.GraphicParameters;

import java.util.LinkedList;
import java.util.List;

class Stage1 implements Stage {

    public Board buildBoard() {
        return new Board();
    }

    public Ball buildBall() {
        return new Ball();
    }

    public List<Block> buildBlocks() {

        List<Block> blockPattern = new LinkedList<>();

        blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL, GraphicParameters.MID_VERTICAL / 2)));
        for (int i = 1; i < 7; i++) {
            for (int j = 1; j < 4; j++) {
                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL - i * 55,
                        GraphicParameters.MID_VERTICAL / 2)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL,
                        GraphicParameters.MID_VERTICAL / 2 - j * 25)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL + i * 55,
                        GraphicParameters.MID_VERTICAL / 2)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL,
                        GraphicParameters.MID_VERTICAL / 2 + j * 25)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL - i * 55,
                        GraphicParameters.MID_VERTICAL / 2 - j * 25)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL + i * 55,
                        GraphicParameters.MID_VERTICAL / 2 - j * 25)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL - i * 55,
                        GraphicParameters.MID_VERTICAL / 2 + j * 25)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL + i * 55,
                        GraphicParameters.MID_VERTICAL / 2 + j * 25)));

            }
        }

        return blockPattern;
    }
}