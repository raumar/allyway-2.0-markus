package de.awacademy.stages;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Movement;
import de.awacademy.game_utilities.Point;
import de.awacademy.model.GraphicParameters;
import javafx.scene.paint.Color;

import java.util.LinkedList;
import java.util.List;

class Stage3 implements Stage {

    public Board buildBoard() {
        return new Board(1500, 200);
    }

    public Ball buildBall() {
        return new Ball(15, new Point(GraphicParameters.MID_HORIZONTAL - 200,
                GraphicParameters.MID_VERTICAL - 180),
                new Movement(550, 700));
    }

    public List<Block> buildBlocks() {

        List<Block> blockPattern = new LinkedList<>();

        for (int j = 0; j < 8; j++) {

            int i = 0;
            while (i * 60 + 25 < GraphicParameters.FIELD_WIDTH / 2) {
                if (j % 2 == 0) {
                    blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL + i * 60,
                            GraphicParameters.FIELD_MAX_UP + 10 + j * 25), Color.DARKCYAN));
                    blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL - i * 60,
                            GraphicParameters.FIELD_MAX_UP + 10 + j * 25), Color.DARKCYAN));
                } else {
                    blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL + i * 55,
                            GraphicParameters.FIELD_MAX_UP + 10 + j * 25)));
                    blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL - i * 55,
                            GraphicParameters.FIELD_MAX_UP + 10 + j * 25)));
                }
                i++;
            }

        }

        return blockPattern;
    }
}
