package de.awacademy.stages;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Movement;
import de.awacademy.game_utilities.Point;
import de.awacademy.model.GraphicParameters;
import javafx.scene.paint.Color;

import java.awt.*;
import java.util.LinkedList;
import java.util.List;

class Stage2 implements Stage {

    public Board buildBoard() {
        return new Board(1200, 300);
    }

    public Ball buildBall() {
        return new Ball(8, new Point(GraphicParameters.FIELD_MAX_LEFT,
                GraphicParameters.MID_VERTICAL),
                new Movement(700, 400));
    }

    public List<Block> buildBlocks() {

        List<Block> blockPattern = new LinkedList<>();

        for (int i = 1; i < 10; i++) {

            blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL,
                    GraphicParameters.FIELD_MAX_UP * 5 + i * 25), Color.FIREBRICK));

            for (int j = 1; j < i; j++) {

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL - j * 55,
                        GraphicParameters.FIELD_MAX_UP * 5 + i * 25)));

                blockPattern.add(new Block(new Point(GraphicParameters.MID_HORIZONTAL + j * 55,
                        GraphicParameters.FIELD_MAX_UP * 5 + i * 25)));

            }
        }

        return blockPattern;
    }
}