package de.awacademy.stages;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Board;

import java.util.List;

interface Stage {

    Board buildBoard();

    Ball buildBall();

    List<Block> buildBlocks();

}
