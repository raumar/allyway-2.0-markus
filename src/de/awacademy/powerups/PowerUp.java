package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_objects.MovingGameObject;
import javafx.scene.paint.Color;

public interface PowerUp extends MovingGameObject {

    Color getColor();
    double getRadius();
    boolean isCollected(Board board);
    void applyPower(Board board, Ball ball);

}
