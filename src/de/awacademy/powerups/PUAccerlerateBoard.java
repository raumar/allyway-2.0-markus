package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUAccerlerateBoard extends AbstractPowerUp {

    public PUAccerlerateBoard(Point position) {
        super(position);
        super.color = Color.FUCHSIA;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        board.changeSpeed(1.2);
    }

}
