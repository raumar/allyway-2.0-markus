package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUShrinkBoard extends AbstractPowerUp {

    public PUShrinkBoard(Point position) {
        super(position);
        super.color = Color.CHARTREUSE;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        board.changeSize(0.8);
    }

}
