package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUGrowBall extends AbstractPowerUp {

    public PUGrowBall(Point position) {
        super(position);
        super.color = Color.DARKVIOLET;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        ball.changeSize(1.4);
    }

}
