package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUShrinkBall extends AbstractPowerUp {

    public PUShrinkBall(Point position) {
        super(position);
        super.color = Color.LIGHTYELLOW;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        ball.changeSize(0.8);
    }

}
