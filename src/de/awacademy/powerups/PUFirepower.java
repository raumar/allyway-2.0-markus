package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUFirepower extends AbstractPowerUp {

    public PUFirepower(Point position) {
        super(position);
        super.color = Color.RED;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        board.giveFirePower();
    }

}
