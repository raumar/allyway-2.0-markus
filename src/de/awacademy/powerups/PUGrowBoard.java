package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUGrowBoard extends AbstractPowerUp {

    public PUGrowBoard(Point position) {
        super(position);
        super.color = Color.TURQUOISE;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        board.changeSize(1.2);
    }

}
