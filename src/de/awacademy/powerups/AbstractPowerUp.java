package de.awacademy.powerups;

import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Movement;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public abstract class AbstractPowerUp implements PowerUp{
    private double radius = 5;
    private Point position;
    private Movement movement = new Movement(0, 100);

    protected Color color = Color.RED;

    public AbstractPowerUp(Point position) {
        this.position = position;
    }

    @Override
    public boolean isCollected(Board board) {
        return position.getyValue() > board.edgeUp() && position.getyValue() < board.edgeDown() && inBoardRange(board);
    }

    private boolean inBoardRange(Board board) {
        return position.getxValue() > board.edgeLeft() && position.getxValue() < board.edgeRight();
    }

    @Override
    public void move(long deltaMillis) {
        //move the ball in direction of current movement
        position.setxValue(position.getxValue() + movement.getxSpeed() * deltaMillis / 1000);
        position.setyValue(position.getyValue() + movement.getySpeed() * deltaMillis / 1000);
    }

    @Override
    public Movement getMovement() {
        return movement;
    }

    @Override
    public double edgeLeft() {
        return position.getxValue() - radius;
    }

    @Override
    public double edgeRight() {
        return position.getxValue() + radius;
    }

    @Override
    public double edgeUp() {
        return position.getyValue() - radius;
    }

    @Override
    public double edgeDown() {
        return position.getyValue() + radius;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public double getRadius() {
        return radius;
    }



}
