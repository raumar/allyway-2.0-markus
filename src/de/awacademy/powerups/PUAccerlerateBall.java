package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUAccerlerateBall extends AbstractPowerUp {

    public PUAccerlerateBall(Point position) {
        super(position);
        super.color = Color.CORAL;
    }



    @Override
    public void applyPower(Board board, Ball ball) {
        ball.changeSpeed(1.2);
    }

}
