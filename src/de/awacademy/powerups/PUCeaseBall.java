package de.awacademy.powerups;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class PUCeaseBall extends AbstractPowerUp {

    public PUCeaseBall(Point position) {
        super(position);
        super.color = Color.DEEPSKYBLUE;
    }


    @Override
    public void applyPower(Board board, Ball ball) {
        ball.changeSpeed(0.8);
    }

}
