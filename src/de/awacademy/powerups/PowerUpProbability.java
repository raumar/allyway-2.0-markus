package de.awacademy.powerups;

public abstract class PowerUpProbability {

    /**
     * Probability for a PowerUp to appear in 1 of VALUE
     */
    private static double value = 100;

    public static double getValue() {
        return value;
    }
}
