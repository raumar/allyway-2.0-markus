package de.awacademy.model;

import de.awacademy.game_objects.*;
import de.awacademy.game_utilities.*;
import de.awacademy.powerups.*;
import de.awacademy.stages.StageBuilder;

import java.util.LinkedList;
import java.util.List;

public class Model {

    private Board board;
    private Ball ball;
    private List<Block> blocks;
    private List<PowerUp> powerUps;
    private List<Bullet> bullets;

    private boolean gameOver = false;
    private boolean stageFinished = false;
    private boolean gameFinishedToTheEnd = false;
    private boolean ballLostButStillAlive = false;
    private boolean gameStart = true;

    private StageBuilder sb;
    private LifeCounter lifeCounter;
    /**
     * time since Stage start in Millis
     */
    long timer = 0L;
    int score = 0;

    public Model() {
        this.sb = new StageBuilder();
        loadStage();
        this.powerUps = new LinkedList<PowerUp>();
        this.bullets = new LinkedList<Bullet>();
        this.lifeCounter = new LifeCounter(3);
    }

    public void update(long deltaMillis) {

        if (!gameStart && !gameOver && !stageFinished && !ballLostButStillAlive) {
            checkLost();
            checkWon();

            moveBoard(board.getSpeed() * deltaMillis / 1000);
            ball.move(deltaMillis);
            movePowerUps(deltaMillis);
            moveBullets(deltaMillis);

            BallBounceFunctionality bbf = new BallBounceFunctionality(this);
            bbf.checkBounces();

            GameplayFunctionality gpf = new GameplayFunctionality(this);
            gpf.handleHitBlocksByBall();
            gpf.handleHitBlocksByBullet();
            gpf.handleCollectedPowerUp();

            timer += deltaMillis;
        }
    }

    public void loadStage() {
        this.board = sb.buildBoard();
        this.ball = sb.buildBall();
        this.blocks = sb.buildBlocks();
        powerUps = new LinkedList<PowerUp>();
        bullets = new LinkedList<Bullet>();
        timer = 0L;

        this.stageFinished = false;
        this.ballLostButStillAlive = false;
    }



    /**
     * method checks, if the game is won and ends the game
     */
    private void checkWon() {
        if (blocks.isEmpty()) {
            if (sb.hasNextStage()) {
                stageFinished = true;
                sb.nextStage();
            } else {
                stageFinished = true;
                gameFinishedToTheEnd = true;
            }
        }

    }

    /**
     * method checks, if the game is lost and ends the game
     */
    private void checkLost() {
        if (ball.getPosition().getyValue() > GraphicParameters.FIELD_MAX_DOWN * 2) {
            lifeCounter.subtractLife(1);
            if (lifeCounter.getLives() == 0) {
                gameOver = true;
            } else {
                ballLostButStillAlive = true;
            }
        }
    }

    void restartGame() {
        sb.reset();
        lifeCounter.reset();
        score = 0;
        loadStage();
        gameOver = false;
        gameStart = true;
    }

    void boardShoot() {

        bullets.add(new Bullet(new Point(board.edgeLeft(), board.edgeUp())));
        bullets.add(new Bullet(new Point(board.edgeRight(), board.edgeUp())));

    }

    private void movePowerUps(long millis) {
        for (PowerUp pu : powerUps) {
            pu.move(millis);
        }
    }

    private void moveBullets(long millis) {
        for (Bullet bullet : bullets) {
            bullet.move(millis);
        }
    }

    private void moveBoard(double pixels) {
        if (ButtonsPressed.left)
            board.move(Direction.LEFT, pixels);
        if (ButtonsPressed.right)
            board.move(Direction.RIGHT, pixels);
    }

    public void killAllBlocks(){
        blocks.clear();
    }

    void startGame() {
        this.gameStart = false;
    }

    public void addBoard(Board board) {
        this.board = board;
    }

    public Ball getBall() {
        return ball;
    }

    public Board getBoard() {
        return board;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public LifeCounter getLifeCounter() {
        return lifeCounter;
    }

    public StageBuilder getStageBuilider() {
        return sb;
    }

    public List<PowerUp> getPowerUps() {
        return powerUps;
    }

    public List<Bullet> getBullets() {
        return bullets;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    boolean isGameOver() {
        return gameOver;
    }

    boolean isStageFinished() {
        return stageFinished;
    }

    boolean isGameFinishedToTheEnd() {
        return gameFinishedToTheEnd;
    }

    boolean isBallLostButStillAlive() {
        return ballLostButStillAlive;
    }

    boolean isGameStart() {
        return gameStart;
    }
}
