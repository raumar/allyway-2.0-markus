package de.awacademy.model;

import de.awacademy.dialogs.*;
import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Bullet;
import de.awacademy.powerups.PowerUp;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.text.DecimalFormat;
import java.text.NumberFormat;


public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {

        if (model.isGameOver()) {
            showDialog(new GameOverDialog(model));

        } else if (model.isStageFinished() && !model.isGameFinishedToTheEnd()) {
            showDialog(new StageFinishedDialog(model));

        } else if (model.isStageFinished() && model.isGameFinishedToTheEnd()) {
            showDialog(new GameFinishedToTheEndDialog(model));

        } else if (model.isBallLostButStillAlive()) {
            showDialog(new BallLostButStillAliveDialog(model));

        } else if (model.isGameStart()) {
            showDialog(new GameStartDialog(model));

        } else {
            drawBackground();
            drawBoard();
            drawBall();
            drawBlocks();
            drawPowerUps();
            drawBullets();
            drawTimer();
            drawScore();
        }

    }

    private void showDialog(GameDialog dialog) {

        drawBackground();

        //draw shadow for fist line
        gc.setFill(GraphicParameters.SHADOW_COLOR);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(new Font("Arial Black", 100));
        gc.fillText(dialog.getFirstLine(), GraphicParameters.MID_HORIZONTAL + GraphicParameters.SHADOW_SHIFT_X * 2,
                GraphicParameters.MID_VERTICAL - 50 + GraphicParameters.SHADOW_SHIFT_Y * 2);

        //draw text
        gc.setFill(dialog.getColor());

        gc.setFont(new Font("Arial Black", 100));
        gc.fillText(dialog.getFirstLine(), GraphicParameters.MID_HORIZONTAL, GraphicParameters.MID_VERTICAL - 50);

        gc.setFont(new Font("Times New Roman", 30));
        gc.fillText(dialog.getSecondLine(), GraphicParameters.MID_HORIZONTAL, GraphicParameters.MID_VERTICAL + 50);

        gc.setFont(new Font("Arial Black", 20));
        gc.fillText(dialog.getOptionalAmend(), GraphicParameters.MID_HORIZONTAL, GraphicParameters.MID_VERTICAL + 90);
    }

    private void drawTimer() {
        gc.setFill(Color.GREY);
        gc.setFont(new Font("Arial", 20));
        gc.setTextAlign(TextAlignment.RIGHT);

        long timerSeconds = model.timer / 1000;
        long timerMinutes = (long) Math.floor(timerSeconds / 60.);
        gc.fillText("TIME: " + timerMinutes + ":" + String.format("%02d", timerSeconds % 60),
                GraphicParameters.FIELD_MAX_RIGHT - 20, GraphicParameters.FIELD_MAX_DOWN - 10);
    }

    private void drawScore() {
        gc.setFill(Color.GREY);
        gc.setFont(new Font("Arial", 20));
        gc.setTextAlign(TextAlignment.LEFT);

        gc.fillText("SCORE: " + model.score,
                GraphicParameters.FIELD_MAX_LEFT + 20, GraphicParameters.FIELD_MAX_DOWN - 10);

    }

    private void drawBullets() {
        for (Bullet bullet : model.getBullets()) {
            gc.setFill(Color.YELLOW);
            gc.fillOval(bullet.getPosition().getxValue() - bullet.getRadius(),
                    bullet.getPosition().getyValue() - bullet.getRadius(),
                    bullet.getRadius() * 2, bullet.getRadius() * 2);
        }
    }

    private void drawPowerUps() {

        for (PowerUp pu : model.getPowerUps()) {
            gc.setFill(pu.getColor());
            gc.fillOval(pu.getPosition().getxValue() - pu.getRadius(),
                    pu.getPosition().getyValue() - pu.getRadius(),
                    pu.getRadius() * 2, pu.getRadius() * 2);
        }
    }

    private void drawBlocks() {
        for (Block block : model.getBlocks()) {

            //shadow
            gc.setFill(GraphicParameters.SHADOW_COLOR);
            gc.fillRect(block.getPosition().getxValue() - block.getWidth() / 2. + GraphicParameters.SHADOW_SHIFT_X,
                    block.getPosition().getyValue() - block.getHeight() / 2. + GraphicParameters.SHADOW_SHIFT_Y,
                    block.getWidth(), block.getHeight());

            //block
            gc.setFill(block.getColor());
            gc.fillRect(block.getPosition().getxValue() - block.getWidth() / 2.,
                    block.getPosition().getyValue() - block.getHeight() / 2.,
                    block.getWidth(), block.getHeight());
        }
    }

    private void drawBall() {

        //shadow
        gc.setFill(GraphicParameters.SHADOW_COLOR);
        gc.fillOval(model.getBall().getPosition().getxValue() - model.getBall().getRadius() + GraphicParameters.SHADOW_SHIFT_X,
                model.getBall().getPosition().getyValue() - model.getBall().getRadius() + GraphicParameters.SHADOW_SHIFT_Y,
                model.getBall().getRadius() * 2, model.getBall().getRadius() * 2);

        //ball
        gc.setFill(Color.WHITE);
        gc.fillOval(model.getBall().getPosition().getxValue() - model.getBall().getRadius(),
                model.getBall().getPosition().getyValue() - model.getBall().getRadius(),
                model.getBall().getRadius() * 2, model.getBall().getRadius() * 2);
    }

    private void drawBoard() {
        //shadow
        gc.setFill(GraphicParameters.SHADOW_COLOR);
        gc.fillRect(model.getBoard().getPosition().getxValue() - model.getBoard().getWidth() / 2. + GraphicParameters.SHADOW_SHIFT_X,
                model.getBoard().getPosition().getyValue() - model.getBoard().getHeight() / 2. + GraphicParameters.SHADOW_SHIFT_Y,
                model.getBoard().getWidth(), model.getBoard().getHeight());

        //board
        gc.setFill(Color.WHITE);
        gc.fillRect(model.getBoard().getPosition().getxValue() - model.getBoard().getWidth() / 2.,
                model.getBoard().getPosition().getyValue() - model.getBoard().getHeight() / 2.,
                model.getBoard().getWidth(), model.getBoard().getHeight());
    }

    private void drawBackground() {

        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, GraphicParameters.CANVAS_WIDTH, GraphicParameters.CANVAS_HEIGHT);
        gc.setFill(Color.BLACK);
        gc.fillRect(GraphicParameters.FRAME_PIXELS, GraphicParameters.FRAME_PIXELS,
                GraphicParameters.CANVAS_WIDTH - GraphicParameters.FRAME_PIXELS * 2,
                GraphicParameters.CANVAS_HEIGHT - GraphicParameters.FRAME_PIXELS * 2);
    }
}
