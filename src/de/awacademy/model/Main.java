package de.awacademy.model;

import de.awacademy.stages.StageBuilder;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Main extends Application {


    private Timer timer;

    @Override
    public void start(Stage primaryStage) throws Exception {

        //prepare Canvas
        Canvas canvas = new Canvas(GraphicParameters.CANVAS_WIDTH, GraphicParameters.CANVAS_HEIGHT);

        Group group = new Group();
        group.getChildren().addAll(canvas);

        Scene scene = new Scene(group);
        primaryStage.setScene(scene);
        primaryStage.show();

        //prepare Model and build Stages
        Model model = new Model();


        //prepare Timer and InputHandler
        timer = new Timer(model, canvas.getGraphicsContext2D());
        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                inputHandler.onKeyPressed(event);
            }
        });

        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                inputHandler.onKeyReleased(event);
            }
        });

        canvas.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                inputHandler.onMouseClicked(event);
            }
        });


        timer.start();
    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}
