package de.awacademy.model;

import de.awacademy.model.Graphics;
import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;

public class Timer extends AnimationTimer {

    private Model model;
    private Graphics graphics;

    private long lastMillis = -1;

    public Timer(Model model, GraphicsContext gc) {
        this.model = model;
        this.graphics = new Graphics(model, gc);;
    }

    @Override
    public void handle(long now) {
        long nowMillis = now / 1000000;

        long deltaMillis = 0;
        if (lastMillis != -1) {
            deltaMillis = nowMillis - lastMillis;
        }

        this.model.update(deltaMillis);
        graphics.draw();

        lastMillis = nowMillis;
    }
}
