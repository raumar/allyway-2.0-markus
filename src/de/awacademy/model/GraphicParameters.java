package de.awacademy.model;

import javafx.scene.paint.Color;

public class GraphicParameters {

    public static final double FRAME_PIXELS = 10;

    public static final double CANVAS_WIDTH = 1000;
    public static final double CANVAS_HEIGHT = 780;

    public static final double MID_HORIZONTAL = CANVAS_WIDTH / 2;
    public static final double MID_VERTICAL = CANVAS_HEIGHT / 2;

    public static final double FIELD_MAX_LEFT = FRAME_PIXELS;
    public static final double FIELD_MAX_RIGHT = CANVAS_WIDTH - FRAME_PIXELS;
    public static final double FIELD_MAX_UP = FRAME_PIXELS;
    public static final double FIELD_MAX_DOWN = CANVAS_HEIGHT - FRAME_PIXELS;

    public static final double FIELD_WIDTH = CANVAS_WIDTH - 2 * FRAME_PIXELS;
    public static final double FIELD_HEIGHT = CANVAS_HEIGHT - 2 * FRAME_PIXELS;

    public static final double SHADOW_SHIFT_X = 3;
    public static final double SHADOW_SHIFT_Y = 2;
    public static final Color SHADOW_COLOR = Color.GRAY;

}
