package de.awacademy.model;

import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyEvent event){
        switch (event.getCode()){
            case TAB:
                //cheat button
                model.killAllBlocks();
                break;
            case LEFT:
                ButtonsPressed.left = true;
                break;
            case RIGHT:
                ButtonsPressed.right = true;
                break;
            case Y:
                if (model.isGameOver()){
                    model.restartGame();
                    break;
                }
            case N:
                if (model.isGameOver()){
                    System.exit(0);
                    break;
                }
            case CONTROL:
                if (model.isGameStart()){
                    model.startGame();
                    break;
                }
                if ((model.isStageFinished() && !model.isGameFinishedToTheEnd()) || model.isBallLostButStillAlive()){
                    model.loadStage();
                    break;
                }
            case SPACE:
                if (model.getBoard().hasFirePower()){
                    model.boardShoot();
                    break;
                }
        }
    }

    public void onKeyReleased(KeyEvent event) {
        switch (event.getCode()){
            case LEFT:
                ButtonsPressed.left = false;
                break;
            case RIGHT:
                ButtonsPressed.right = false;
                break;
        }
    }

    public void onMouseClicked(MouseEvent event) {

    }
}
