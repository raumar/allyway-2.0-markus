package de.awacademy.dialogs;

import de.awacademy.model.Model;
import javafx.scene.paint.Color;

public class GameFinishedToTheEndDialog extends AbstractDialog {

    public GameFinishedToTheEndDialog(Model model) {
        this("DEIN SCORE: " +model.getScore());
    }

    public GameFinishedToTheEndDialog(String textAmend) {
        super.color = Color.GOLD;
        firstLine = "GLÜCKWUNSCH";
        secondLine = "Du hast alle Levels geschafft !!!";
        optionalAmend = textAmend;
    }
}
