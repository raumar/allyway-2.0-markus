package de.awacademy.dialogs;

import javafx.scene.paint.Color;

public interface GameDialog {

    String getFirstLine();
    String getSecondLine();
    String getOptionalAmend();
    Color getColor();

}
