package de.awacademy.dialogs;

import de.awacademy.model.Model;
import javafx.scene.paint.Color;

public class GameOverDialog extends AbstractDialog {

    public GameOverDialog(Model model) {
        this("");
    }

    public GameOverDialog(String textAmend) {
        super.color = Color.RED;
        firstLine = "GAME OVER";
        secondLine = "Möchtest du neu beginnen? (y/n)";
        optionalAmend = textAmend;
    }
}
