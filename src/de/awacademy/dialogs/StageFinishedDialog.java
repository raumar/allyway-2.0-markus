package de.awacademy.dialogs;

import de.awacademy.model.Model;
import javafx.scene.paint.Color;

public class StageFinishedDialog extends AbstractDialog {

    public StageFinishedDialog(Model model) {
        this("...zu Level " +
                (3 - model.getStageBuilider().getStages().size()));
    }

    public StageFinishedDialog(String textAmend) {
        super.color = Color.GREENYELLOW;
        firstLine = "LEVEL BEENDET";
        secondLine = "Mit [STRG] geht's weiter...";
        optionalAmend = textAmend;
    }
}
