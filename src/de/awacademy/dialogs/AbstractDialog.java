package de.awacademy.dialogs;

import javafx.scene.paint.Color;

public abstract class AbstractDialog implements GameDialog{

    String firstLine = "";
    String secondLine = "";
    String optionalAmend = "";

    protected Color color = Color.RED;

    public String getFirstLine() {
        return firstLine;
    }

    public String getSecondLine() {
        return secondLine;
    }

    public String getOptionalAmend() {
        return optionalAmend;
    }

    @Override
    public Color getColor() {
        return color;
    }

    public void setOptionalAmend(String optionalAmend) {
        this.optionalAmend = optionalAmend;
    }
}
