package de.awacademy.dialogs;

import de.awacademy.model.Model;
import javafx.scene.paint.Color;

public class BallLostButStillAliveDialog extends AbstractDialog {

    public BallLostButStillAliveDialog(Model model) {
        this("noch " + model.getLifeCounter().getLives() + " Leben übrig");
    }

    public BallLostButStillAliveDialog(String textAmend) {
        super.color = Color.ORANGE;
        firstLine = "BALL VERLOREN";
        secondLine = "Mit [STRG] kannst du es nochmal versuchen...";
        optionalAmend = textAmend;
    }
}