package de.awacademy.dialogs;

import de.awacademy.model.Model;
import javafx.scene.paint.Color;

public class GameStartDialog extends AbstractDialog {

    public GameStartDialog(Model model) {
        this("DAS COOLSTE SPIEL DER WELT");
    }

    public GameStartDialog(String textAmend) {
        super.color = Color.GOLD;
        firstLine = "ALLYWAY 2.0";
        secondLine = "Mit [STRG] geht's los!";
        optionalAmend = textAmend;
    }
}
