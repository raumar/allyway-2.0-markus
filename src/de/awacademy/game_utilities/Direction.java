package de.awacademy.game_utilities;

public enum Direction {
    UP, DOWN, LEFT, RIGHT
}
