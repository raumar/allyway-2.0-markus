package de.awacademy.game_utilities;

import java.util.Optional;

public class LifeCounter {

    private final int initialLives;
    private int lives;

    public LifeCounter(int initialLives) {
        this.initialLives = initialLives;
        this.lives = initialLives;
    }

    public void reset(){
        lives = initialLives;
    }

    public void subtractLife(int lives){
        this.lives -= lives;
    }

    public void addLife(int lives){
        this.lives += lives;
    }

    public int getLives() {
        return lives;
    }
}
