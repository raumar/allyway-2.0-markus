package de.awacademy.game_utilities;

import de.awacademy.game_objects.Ball;
import de.awacademy.game_objects.Board;
import de.awacademy.model.GraphicParameters;
import de.awacademy.model.Model;

public class BallBounceFunctionality {

    private Ball ball;
    private Board board;

    public BallBounceFunctionality(Model model) {
        this.ball = model.getBall();
        this.board = model.getBoard();
    }

    /**
     * checks for ball bounces against frame and board
     */
    public void checkBounces() {
        //check for bounce frame
        bounceFrame();
        //check for bounce board
        bounceBoard();
    }

    /**
     * methods checks, if Ball bounces the Board and change movement respectively
     *
     * @return true, if there is a bounce with the Board
     */
    private boolean bounceBoard() {
        boolean action = false;
        //define a range of pixels which represent a hit as 3% of BallSpeed -> tested for running processor
        if ((ball.edgeDown() > board.edgeUp() && ball.edgeDown() < board.edgeUp() + ball.getSpeed() * 0.05)
                && ballMovementVertical() == Direction.DOWN && ballInBoardRange()) {
            bounceHorizontal();
            bounceBoardDynamic();
            action = true;
        }
        return action;
    }

    private boolean ballInBoardRange() {
        return ball.getPosition().getxValue() > board.edgeLeft() && ball.getPosition().getxValue() < board.edgeRight();
    }

    private Direction ballMovementVertical() {
        if (ball.getMovement().getySpeed() > 0) {
            return Direction.DOWN;
        } else {
            return Direction.UP;
        }
    }

    private Direction ballMovementHorizontal() {
        if (ball.getMovement().getxSpeed() > 0) {
            return Direction.RIGHT;
        } else {
            return Direction.LEFT;
        }
    }

    /**
     * methods checks, if Ball bounces the Field Frame and change movement respectively
     *
     * @return true, if there is a bounce with the Field Frame
     */
    private boolean bounceFrame() {
        boolean action = false;
        if (ball.edgeLeft() < GraphicParameters.FIELD_MAX_LEFT && ball.getMovement().getxSpeed() < 0) {
            bounceVertical();
            action = true;
        }
        if (ball.edgeRight() > GraphicParameters.FIELD_MAX_RIGHT && ball.getMovement().getxSpeed() > 0) {
            bounceVertical();
            action = true;
        }
        if (ball.edgeUp() < GraphicParameters.FIELD_MAX_UP && ball.getMovement().getySpeed() < 0) {
            bounceHorizontal();
            action = true;
        }
        return action;
    }

    /**
     * method represents a vertical bounce by reverting the movement in x-direction
     */
    private void bounceVertical() {
        ball.getMovement().setxSpeed(-ball.getMovement().getxSpeed());
    }

    /**
     * method represents a hoizontal bounce by reverting the movement in y-direction
     */
    private void bounceHorizontal() {
        ball.getMovement().setySpeed(-ball.getMovement().getySpeed());
    }

    private void bounceBoardDynamic() {

        double speed = ball.calculateCurrentSpeed();
        double deltaX = ball.getPosition().getxValue() - board.getPosition().getxValue();

        double newXSpeed = ball.getMovement().getxSpeed() + deltaX;
        if (newXSpeed > speed) {
            newXSpeed = speed * 0.9;
        } else if (newXSpeed < -speed) {
            newXSpeed = -speed * 0.9;
        }

        ball.getMovement().setxSpeed(newXSpeed);
        ball.getMovement().setySpeed(-(Math.sqrt(Math.pow(speed, 2) - Math.pow(newXSpeed, 2))));
    }


}
