package de.awacademy.game_utilities;

import java.util.Random;

public class Movement {

    /**
     * creates a random movement respecting speed
     * @param speed speed of the movement in pixels / second
     * @return random movement
     */
    public static Movement randomMovement(int speed) {
        Movement movement = new Movement();
        Random rand = new Random();
        int xSpeed = rand.nextInt(speed / 2) + speed / 2;
        movement.setxSpeed((double) xSpeed);

        double ySpeed = Math.sqrt(Math.pow(speed,2)-Math.pow(xSpeed,2));
        movement.setySpeed(xSpeed);
        return movement;
    }

    /**
     * Movement in x-direction (horizontal)
     * measured in pixel / second
     */
    private double xSpeed;

    /**
     * Movement in y-direction (vertical)
     * measured in pixel / second
     */
    private double ySpeed;

    public Movement() {
    }

    ;

    public Movement(double xSpeed, double ySpeed) {
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    public double getxSpeed() {
        return xSpeed;
    }

    public void setxSpeed(double xSpeed) {
        this.xSpeed = xSpeed;
    }

    public double getySpeed() {
        return ySpeed;
    }

    public void setySpeed(double ySpeed) {
        this.ySpeed = ySpeed;
    }
}
