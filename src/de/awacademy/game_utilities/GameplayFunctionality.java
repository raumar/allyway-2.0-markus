package de.awacademy.game_utilities;

import de.awacademy.game_objects.Block;
import de.awacademy.game_objects.Bullet;
import de.awacademy.model.Model;
import de.awacademy.powerups.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GameplayFunctionality {

    private Model model;

    public GameplayFunctionality(Model model) {
        this.model = model;
    }

    public void handleCollectedPowerUp() {
        PowerUp collectedPU = null;
        for (PowerUp pu : model.getPowerUps()) {
            if (pu.isCollected(model.getBoard())) {
                collectedPU = pu;
                pu.applyPower(model.getBoard(), model.getBall());
            }
        }
        model.getPowerUps().remove(collectedPU);
    }

    /**
     * method checks for blocks hit by the ball, removes them and randomly creates PowerUps
     */
    public void handleHitBlocksByBall() {
        List<Block> hitBlocks = new LinkedList<>();
        for (Block block : model.getBlocks()) {
            if (block.isHitByBall(model.getBall())) {
                hitBlocks.add(block);
                createPowerUpOnRandom(block);
                model.setScore(model.getScore() + 10);
            }
        }
        model.getBlocks().removeAll(hitBlocks);
    }

    /**
     * method checks for blocks hit by the ball, removes them and randomly creates PowerUps
     */
    public void handleHitBlocksByBullet() {
        List<Block> hitBlocks = new LinkedList<>();
        List<Bullet> hitBullets = new LinkedList<>();

        for (Block block : model.getBlocks()) {
            for (Bullet bullet : model.getBullets()) {
                if (block.isHitByBullet(bullet)) {
                    hitBlocks.add(block);
                    hitBullets.add(bullet);
                    createPowerUpOnRandom(block);
                    model.setScore(model.getScore() + 10);
                }
            }
        }
        model.getBlocks().removeAll(hitBlocks);
        model.getBullets().removeAll(hitBullets);
    }

    private void createPowerUpOnRandom(Block block) {

        PowerUp pu = null;
        Random rand = new Random();
        int randValue = rand.nextInt((int) (PowerUpProbability.getValue()));
        switch (randValue) {
            case 1:
                pu = new PUAccerlerateBoard(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 2:
                pu = new PUCeaseBoard(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 3:
                pu = new PUGrowBoard(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 4:
                pu = new PUShrinkBoard(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                model.getPowerUps().add(pu);
            case 5:
                pu = new PUAccerlerateBall(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 6:
                pu = new PUCeaseBall(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 7:
                pu = new PUGrowBall(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 8:
                pu = new PUShrinkBall(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;
            case 9:
                pu = new PUFirepower(new Point(block.getPosition().getxValue(), block.getPosition().getyValue()));
                break;

        }
        if (pu!=null) {
            model.getPowerUps().add(pu);
        }
    }

}
