package de.awacademy.game_utilities;

import java.util.Objects;

public class Point {

    private double xValue;
    private double yValue;

    public Point(){};

    public Point(double xValue, double yValue) {
        this.xValue = xValue;
        this.yValue = yValue;
    }

    public double distance(Point point) {
        return Math.sqrt(Math.pow(xValue - point.getxValue(), 2) + Math.pow(yValue - point.getyValue(), 2));
    }

    public double getxValue() {
        return xValue;
    }

    public void setxValue(double xValue) {
        this.xValue = xValue;
    }

    public double getyValue() {
        return yValue;
    }

    public void setyValue(double yValue) {
        this.yValue = yValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return xValue == point.xValue &&
                yValue == point.yValue;
    }

    @Override
    public int hashCode() {
        return Objects.hash(xValue, yValue);
    }
}
