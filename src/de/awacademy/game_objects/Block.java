package de.awacademy.game_objects;

import de.awacademy.game_utilities.Point;
import javafx.scene.paint.Color;

public class Block implements GameObject {

    private int width = 50;
    private int height = 20;
    private Point position;

    private Color color = Color.BISQUE;


    public Block(Point position) {
        this.position = position;
    }

    public Block(Point position, Color color) {
        this(position);
        this.color = color;
    }

    public Block(Point position, int width, int height) {
        this(position);
        this.width = width;
        this.height = height;
    }

    public boolean isHitByBall(Ball ball) {

        Point corner = new Point();
        //up-left
        corner.setxValue(position.getxValue() - width / 2.);
        corner.setyValue(position.getyValue() - height / 2.);
        if (corner.distance(ball.getPosition()) < ball.getRadius()) {
            return true;
        }
        //down-left
        corner.setxValue(position.getxValue() - width / 2.);
        corner.setyValue(position.getyValue() + height / 2.);
        if (corner.distance(ball.getPosition()) < ball.getRadius()) {
            return true;
        }
        //up-right
        corner.setxValue(position.getxValue() + width / 2.);
        corner.setyValue(position.getyValue() - height / 2.);
        if (corner.distance(ball.getPosition()) < ball.getRadius()) {
            return true;
        }
        //down-right
        corner.setxValue(position.getxValue() + width / 2.);
        corner.setyValue(position.getyValue() + height / 2.);
        if (corner.distance(ball.getPosition()) < ball.getRadius()) {
            return true;
        }
        //right through
        boolean centerBallThroughBlock = edgeDown() > ball.getPosition().getyValue() && edgeUp() < ball.getPosition().getyValue() &&
                edgeRight() > ball.getPosition().getxValue() && edgeLeft() < ball.getPosition().getxValue();
        if (centerBallThroughBlock) {
            return true;
        }

        return false;
    }

    public boolean isHitByBullet(Bullet bullet) {

        return edgeDown() > bullet.edgeUp() && edgeRight() > bullet.edgeLeft() && edgeLeft() < bullet.edgeRight();
    }

    public Point getPosition() {
        return position;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double edgeLeft() {
        return position.getxValue() - width / 2.;
    }

    public double edgeRight() {
        return position.getxValue() + width / 2.;
    }

    @Override
    public double edgeUp() {
        return position.getyValue() - height / 2.;
    }

    @Override
    public double edgeDown() {
        return position.getyValue() + height / 2.;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

}
