package de.awacademy.game_objects;

import de.awacademy.game_utilities.Point;

public interface GameObject {

    //methods returnig the ultimate point of the GameObject
    double edgeLeft();
    double edgeRight();
    double edgeUp();
    double edgeDown();

    Point getPosition();

}
