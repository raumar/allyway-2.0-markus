package de.awacademy.game_objects;

import de.awacademy.game_utilities.Movement;
import de.awacademy.game_utilities.Point;
import de.awacademy.model.GraphicParameters;

import java.util.Spliterator;

public class Ball implements MovingGameObject {

    /**
     * size of the Ball represented as radius in pixels
     */
    private double radius = 10;
    private Point position = new Point(GraphicParameters.FIELD_MAX_LEFT + radius, GraphicParameters.MID_VERTICAL / 2);
    private Movement movement = new Movement(600, 500);

    private double speed;

    private final double SPEED_MIN = 500;
    private final double SPEED_MAX = 1500;
    private final double RADIUS_MAX = 25;
    private final double RADIUS_MIN = 5;

    public Ball() {
        calculateCurrentSpeed();
    }

    public Ball(double radius) {
        this();
        this.radius = radius;
    }

    public Ball(Point position, Movement movement) {
        this.position = position;
        this.movement = movement;

        calculateCurrentSpeed();
    }

    public Ball(double radius, Point position, Movement movement) {
        this(position, movement);
        this.radius = radius;
    }

    @Override
    public void move(long deltaMillis) {
        position.setxValue(position.getxValue() + movement.getxSpeed() * deltaMillis / 1000);
        position.setyValue(position.getyValue() + movement.getySpeed() * deltaMillis / 1000);
    }

    /**
     * method to increase and decrease speed
     *
     * @param factor fraction of speed after change relative to speed before change
     */
    public void changeSpeed(double factor) {
        calculateCurrentSpeed();
        movement.setxSpeed(movement.getxSpeed() * factor);
        movement.setySpeed(movement.getySpeed() * factor);

        double newSpeed = Math.sqrt((Math.pow(movement.getxSpeed(), 2) + Math.pow(movement.getySpeed(), 2)));
        double bufferFactor = 1;
        if (newSpeed < SPEED_MIN) {
            bufferFactor = SPEED_MIN / newSpeed;
        } else if (newSpeed > SPEED_MAX) {
            bufferFactor = SPEED_MAX / newSpeed;
        }

        movement.setxSpeed(movement.getxSpeed() * bufferFactor);
        movement.setySpeed(movement.getySpeed() * bufferFactor);
        calculateCurrentSpeed();
    }

    /**
     * Calculates current speed by using Pythagoras
     */
    public double calculateCurrentSpeed() {
        speed = Math.sqrt((Math.pow(movement.getxSpeed(), 2) + Math.pow(movement.getySpeed(), 2)));
        return speed;
    }

    public void changeSize(double factor) {
        radius *= factor;
        radius = Math.max(radius, RADIUS_MIN);
        radius = Math.min(radius, RADIUS_MAX);
    }

    @Override
    public Movement getMovement() {
        return movement;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    public double getSpeed() {
        return speed;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double edgeLeft() {
        return position.getxValue() - radius;
    }

    @Override
    public double edgeRight() {
        return position.getxValue() + radius;
    }

    @Override
    public double edgeUp() {
        return position.getyValue() - radius;
    }

    @Override
    public double edgeDown() {
        return position.getyValue() + radius;
    }
}
