package de.awacademy.game_objects;

import de.awacademy.game_utilities.Movement;

/**
 * Object that have a auto-movement within
 */
public interface MovingGameObject extends GameObject {

    /**
     * Shift position of the GameObject in direction of movement
     * @param deltaMillis time in milliseconds since last move
     */
    void move(long deltaMillis);

    Movement getMovement();

        //enter internal variable speed for calculating movement without changing ball speed
}
