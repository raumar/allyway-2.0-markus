package de.awacademy.game_objects;

import de.awacademy.game_utilities.Movement;
import de.awacademy.game_utilities.Point;
import de.awacademy.model.GraphicParameters;

public class Bullet implements MovingGameObject {

    private double radius = 2;
    private Point position;
    private Movement movement = new Movement(0, -1000);

    public Bullet(Point position) {
        this.position = position;
    }

    @Override
    public void move(long deltaMillis) {
        position.setxValue(position.getxValue() + movement.getxSpeed() * deltaMillis / 1000);
        position.setyValue(position.getyValue() + movement.getySpeed() * deltaMillis / 1000);
    }

    @Override
    public Movement getMovement() {
        return movement;
    }

    @Override
    public double edgeLeft() {
        return position.getxValue() - radius;
    }

    @Override
    public double edgeRight() {
        return position.getxValue() + radius;
    }

    @Override
    public double edgeUp() {
        return position.getyValue() - radius;
    }

    @Override
    public double edgeDown() {
        return position.getyValue() + radius;
    }

    @Override
    public Point getPosition() {
        return position;
    }

    public double getRadius() {
        return radius;
    }
}
