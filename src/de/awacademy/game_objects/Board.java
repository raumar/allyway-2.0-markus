package de.awacademy.game_objects;

import de.awacademy.game_utilities.Direction;
import de.awacademy.model.GraphicParameters;
import de.awacademy.game_utilities.Point;

public class Board implements GameObject {

    private int width = 200;
    private int height = 20;
    private Point position = new Point(GraphicParameters.MID_HORIZONTAL, GraphicParameters.FIELD_MAX_DOWN - 40);
    /**
     * Speed in pixels / second
     */
    private double speed = 800;

    private final double SPEED_MIN = 500;
    private final double SPEED_MAX = 2000;
    private final int WIDTH_MAX = 400;
    private final int WIDTH_MIN = 100;

    private boolean firePower = false;

    public Board() {
    }

    public Board(double speed, int size) {
        this.speed = speed;
        this.width = size;
    }

    public void move(Direction direction, double pixels) {
        switch (direction) {
            case LEFT:
                if (edgeLeft() > GraphicParameters.FIELD_MAX_LEFT) {
                    position.setxValue(position.getxValue() - pixels);
                }
                break;
            case RIGHT:
                if (edgeRight() < GraphicParameters.FIELD_MAX_RIGHT) {
                    position.setxValue(position.getxValue() + pixels);
                }
                break;
        }
    }

    public void changeSpeed(double factor){
        speed *= factor;
        speed = Math.max(speed, SPEED_MIN);
        speed = Math.min(speed, SPEED_MAX);
    }

    public void changeSize(double factor){
        width *= factor;
        width = Math.max(width, WIDTH_MIN);
        width = Math.min(width, WIDTH_MAX);
    }

    public Point getPosition() {
        return position;
    }

    public double getSpeed() {
        return speed;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean hasFirePower() {
        return firePower;
    }

    public void giveFirePower() {
        this.firePower = true;
    }

    public double edgeLeft() {
        return position.getxValue() - width / 2.;
    }

    public double edgeRight() {
        return position.getxValue() + width / 2.;
    }

    @Override
    public double edgeUp() {
        return position.getyValue() - height / 2.;
    }

    @Override
    public double edgeDown() {
        return position.getyValue() + height / 2.;
    }


}
